
/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package es.udc.fic.ri.p1;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Date;
import java.util.Properties;
import java.io.Reader;
import java.net.InetAddress;
import java.io.FileNotFoundException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.LongPoint;
import org.apache.lucene.document.DateTools;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;


/** Index all text files under a directory.
 * <p>
 * This is a command-line application demonstrating simple Lucene indexing.
 * Run it with no command-line arguments for usage information.
 */
public class IndexFiles {
  
  private IndexFiles() {}
  
  public static Properties getPropValues() throws IOException {
	  String result = "";
	  InputStream inputStream = null;
	  Properties prop = null;
	  
	  try {		  
		  prop = new Properties();
		  
		  String propFileName = "config.properties";
		  
		  inputStream = new FileInputStream(System.getProperty("user.dir") + "/config.properties");
		  
		  if (inputStream != null) {
			  prop.load(inputStream);
		  } else {
			  throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
		  }
		  
		  Date time = new Date(System.currentTimeMillis());
		  
		  // get the property value and print it out
		  String docs = prop.getProperty("user");
		  String partialIndexes = prop.getProperty("partialIndexes");
		  String onlyFiles = prop.getProperty("onlyFiles");
		  String onlyTopLines = prop.getProperty("onlyTopLines");
		  String onlyBottomLines = prop.getProperty("onlyBottomLines");
		  
	  } catch (Exception e) {
		  System.out.println("Exception: " + e);
	  } finally {
		  inputStream.close();
	  }
	  return prop;
  }

 

  /**
   * Indexes the given file using the given writer, or if a directory is given,
   * recurses over files and directories found under the given directory.
   * 
   * NOTE: This method indexes one document per input file.  This is slow.  For good
   * throughput, put multiple documents into your input file(s).  An example of this is
   * in the benchmark module, which can create "line doc" files, one document per line,
   * using the
   * <a href="../../../../../contrib-benchmark/org/apache/lucene/benchmark/byTask/tasks/WriteLineDocTask.html"
   * >WriteLineDocTask</a>.
   *  
   * @param writer Writer to the index where the given file/dir info will be stored
   * @param path The file to index, or the directory to recurse into to find files to index
   * @throws IOException If there is a low-level I/O error
   */
  static void indexDocs(final IndexWriter writer, Path path, String fileExtensions) throws IOException {
	  String[] extensions = null;
	  
	if (fileExtensions != null) {
		 extensions = fileExtensions.split(" ");
	} 
	  
    if (Files.isDirectory(path)) {
    	if (!(fileExtensions == null)) {
  		  for (int i = 0; i < extensions.length; i++) {
  			  if (path.toString().toLowerCase().endsWith(extensions[i])) {
  				Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
  			        @Override
  			        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
  			          try {
  			        	  
  			        	  indexDoc(writer, file, attrs.lastModifiedTime().toMillis());
  			          } catch (IOException ignore) {
  			        	  // don't index files that can't be read.
  			          }
  			          return FileVisitResult.CONTINUE;
  			        }
  			      });
  			    } else {
  			      indexDoc(writer, path, Files.getLastModifiedTime(path).toMillis());
  			    }
  			  }
  		  }
  	  }
      
  }
  
  /**
   * This Runnable takes a folder an prints its path.
   */
  public static class WorkerThread implements Runnable {
	  private final Path folder;
	  
	  public WorkerThread(final Path folder) {
		  this.folder = folder;
	  }
	  
	  @Override
	  public void run() {
		  System.out.println(String.format("I am the thread '%s' and I am responsible for folder '%s'", Thread.currentThread().getName(), folder));
	  }
  }
  
  
  
  
  /** Indexes a single document */
  static void indexDoc(IndexWriter writer, Path file, long lastModified) throws IOException {
    try (InputStream stream = Files.newInputStream(file)) {
      // make a new, empty document
      Document doc = new Document();
      BasicFileAttributes attr = Files.readAttributes(file, BasicFileAttributes.class);
      
      // Add the path of the file as a field named "path".  Use a
      // field that is indexed (i.e. searchable), but don't tokenize 
      // the field into separate words and don't index term frequency
      // or positional information:
      Field pathField = new StringField("path", file.toString(), Field.Store.YES);
      doc.add(pathField);
      
      // Add the last modified date of the file a field named "modified".
      // Use a LongPoint that is indexed (i.e. efficiently filterable with
      // PointRangeQuery).  This indexes to milli-second resolution, which
      // is often too fine.  You could instead create a number based on
      // year/month/day/hour/minutes/seconds, down the resolution you require.
      // For example the long value 2011021714 would mean
      // February 17, 2011, 2-3 PM.
      doc.add(new LongPoint("modified", lastModified));
      
      // Add the contents of the file to a field named "contents".  Specify a Reader,
      // so that the text of the file is tokenized and indexed, but not stored.
      // Note that FileReader expects the file to be in UTF-8 encoding.
      // If that's not the case searching for special characters will fail.
      doc.add(new TextField("contents", new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8))));
      
      // Now, we need to ad both hostname and thread properties.
      Field hostField = new StringField("host", InetAddress.getLocalHost().getHostName().toString(), Field.Store.YES);
      doc.add(hostField);
      Field threadField = new StringField("thread", Thread.currentThread().getName().toString(), Field.Store.YES);
      doc.add(threadField);
      Field sizeField = new StringField("sizeKb", Long.toString(attr.size() / 1024), Field.Store.NO);
      doc.add(sizeField);
      
      DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
      
      
      Field creationField = new StringField("creationTime", df.format(attr.creationTime().toMillis()), Field.Store.NO);
      doc.add(creationField);
      doc.add(new StringField("lastAccessTime", df.format(attr.lastAccessTime().toMillis()), Field.Store.NO));
      doc.add(new StringField("lastModifiedTime", df.format(attr.lastModifiedTime().toMillis()), Field.Store.NO));
      doc.add(new StringField("creationTimeLucene", DateTools.dateToString(new Date(attr.creationTime().toMillis()), DateTools.Resolution.MILLISECOND), Field.Store.YES));
      doc.add(new StringField("lastAccessTimeLucene", DateTools.dateToString(new Date(attr.lastAccessTime().toMillis()), DateTools.Resolution.MILLISECOND), Field.Store.YES));
      doc.add(new StringField("lastModifiedTimeLucene", DateTools.dateToString(new Date(attr.lastModifiedTime().toMillis()), DateTools.Resolution.MILLISECOND), Field.Store.YES));
      
      if (writer.getConfig().getOpenMode() == OpenMode.CREATE) {
        // New index, so we just add the document (no old document can be there):
        System.out.println("adding " + file);
        writer.addDocument(doc);
      } else {
        // Existing index (an old copy of this document may have been indexed) so 
        // we use updateDocument instead to replace the old one matching the exact 
        // path, if present:
        System.out.println("updating " + file);
        writer.updateDocument(new Term("path", file.toString()), doc);
      }
    }
  }
  
  /** Index all text files under a directory. */
  public static void main(String[] args) {
    String usage = "java org.apache.lucene.demo.IndexFiles"
                 + " [-index INDEX_PATH] [-docs DOCS_PATH] [-update]\n\n"
                 + "This indexes the documents in DOCS_PATH, creating a Lucene index"
                 + "in INDEX_PATH that can be searched with SearchFiles";
    String indexPath = "index";
    String docsPath = null;
    int nThreads = 0; 
    String mode = "";
    String fileExtensions = "";
    
    
    boolean create = true;
    try {
    	Properties properties = getPropValues();
    	for(int i=0;i<args.length;i++) {
    	  if ("-index".equals(args[i])) {
    	    indexPath = args[i+1];
    	    i++;
       // } else if ("-docs".equals(args[i])) {
       //   docsPath = args[i+1];
       //   i++;  
    	  } else if ("-update".equals(args[i])) {
    	    create = false;
    	  } else if ("-nThreads".equals(args[i])) {
    		nThreads = Integer.parseInt(args[i+1]);
    	  } else if ("-openmode".equals(args[i])) {
    	    mode = args[i+1];  
    	  } else if ("-onlyFiles".equals(args[i])) {
    	    fileExtensions = properties.getProperty("onlyFiles");
    	    if (fileExtensions.isBlank()) {
    	    	fileExtensions = null;
    	    }
    	  }
    	}
    } catch (Exception e) {
    	System.out.println(" caught a " + e.getClass() +
    		       "\n with message: " + e.getMessage());
    }
    
    
    if (nThreads == 0) {
    	nThreads = Runtime.getRuntime().availableProcessors();
    }
    
    final ExecutorService executor = Executors.newFixedThreadPool(nThreads);


    if (docsPath == null) {
      System.err.println("Usage: " + usage);
      System.exit(1);
    }

    final Path docDir = Paths.get(docsPath);
    if (!Files.isReadable(docDir)) {
      System.out.println("Document directory '" +docDir.toAbsolutePath()+ "' does not exist or is not readable, please check the path");
      System.exit(1);
    }
    
    Date start = new Date();
    try {
      System.out.println("Indexing to directory '" + indexPath + "'...");

      Directory dir = FSDirectory.open(Paths.get(indexPath));
      Analyzer analyzer = new StandardAnalyzer();
      IndexWriterConfig iwc = new IndexWriterConfig(analyzer);

      if (create) {
        // Create a new index in the directory, removing any
       // previously indexed documents:
        iwc.setOpenMode(OpenMode.CREATE);
      } else {
    	 if (mode.equals("append")) {
    		iwc.setOpenMode(OpenMode.APPEND); 
    	 } else {
    		// Add new documents to an existing index:
    		iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
    	 }
      }

      // Optional: for better indexing performance, if you
      // are indexing many documents, increase the RAM
      // buffer.  But if you do this, increase the max heap
      // size to the JVM (eg add -Xmx512m or -Xmx1g):
      //
      // iwc.setRAMBufferSizeMB(256.0);

      IndexWriter writer = new IndexWriter(dir, iwc);
      indexDocs(writer, docDir, fileExtensions);

      // NOTE: if you want to maximize search performance,
      // you can optionally call forceMerge here.  This can be
      // a terribly costly operation, so generally it's only
      // worth it when your index is relatively static (ie
      // you're done adding documents to it):
      //
      // writer.forceMerge(1);

      writer.close();

      Date end = new Date();
      System.out.println(end.getTime() - start.getTime() + " total milliseconds");

    } catch (IOException e) {
      System.out.println(" caught a " + e.getClass() +
       "\n with message: " + e.getMessage());
    }
  }

}




