package es.udc.fic.ri.p1;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;

import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.FieldInfo;
import org.apache.lucene.index.FieldInfos;
import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.index.MultiTerms;
import org.apache.lucene.index.PostingsEnum;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.BytesRef;
import org.apache.lucene.search.CollectionStatistics;
import org.apache.lucene.search.IndexSearcher;

public class StatsField {
	
	public void main (String[] args) {
		String usage = "java -jar SimilarTerms-0.0.1-SNAPSHOT-jar-with-dependencies.jar" + 
					"[-index <index_folder>] [-field <field_name>]";
		String indexPath = "";
		String field = "";
		Directory dir = null;
		DirectoryReader reader = null;
		long docCount;
		long maxDoc;
		long sumDocFreq;
		long sumTotalTermFreq;
		
		for(int i=0;i<args.length;i++) {
			if("-index".equals(args[i])) {
				indexPath = args[i+1];
				i++;
			}
			else if ("-field".equals(args[i])) {
				field = args[i+1];
				i++;
			}
		}
		
		if(indexPath != null) {
			if("".equals(indexPath)) {
				System.out.println("No indices found in this path, usage: " + usage);
				System.exit(1);
			}
		} else {
			System.err.println("Path to index needed, usage: " + usage);
			System.exit(1);
		}
		try {
			
			if(!("".equals(field))) {
				IndexSearcher searcher = new IndexSearcher(reader);
				CollectionStatistics colStats = searcher.collectionStatistics(field);
				maxDoc = colStats.maxDoc();
				docCount = colStats.docCount();
				sumDocFreq = colStats.sumDocFreq();
				sumTotalTermFreq = colStats.sumTotalTermFreq();
				System.out.println("Field " + field +" appears on " + docCount + " at least once" +
								", has a total of " + sumDocFreq + " postings and there are " + 
								sumTotalTermFreq + "tokens for this field.");
			}
			else {
				dir = FSDirectory.open(Paths.get(indexPath));
				reader = DirectoryReader.open(dir);
			}
		}catch (IOException e) {
			System.out.println("Unable to read index" + e);
			e.printStackTrace();
		}
	}
}
